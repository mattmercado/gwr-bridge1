import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
 
  appTitle: string = 'GWRBridge'
  featureTitle: string = 'SDWIS/STATE Bridge'
  names: string[] = ['', 'Positive SAR List', 'Migrate Designated Candidates']
  NavigationSelection: string;
  ReturnedRows: number;
  postData = {
    test: 'testcontent'
  }
  /*url = 'http://10.62.1.15:8006/GWRws/services/GWR?WSDL';
  //this.postData
  constructor(private http:HttpClient) { 
    this.http.post(this.url,this.postData).toPromise().then(data => {
      console.log("data" + JSON.stringify(data));
    })
  }*/

  ngOnInit(): void {
  }
  onSubmitNav(inp): void {
    //CHANGE Navigation Variable
    
    //this.NavigationSelection = "Positive SAR List";
    this.NavigationSelection =  inp.value;

    if(inp==='Migrated Designated Candidates'){
      this.NavigationSelection='Positive SAR List'
    } 
    console.log("change!")
  }
}
